
window.onload = init()


function init() {
	const data = getData();

	data.forEach( info => { addCard(info) })
	
	console.log(data)
	
}

function addCard ({firstName, lastName, job, location, profile}) {
	const container = document.getElementById('name-card-items');
	const card = document.createElement('LI');

	card.innerHTML = `
	<li class="card card-trans">
		<figure class="card-content">
			<div class="img-and-info">
				<img src=${profile} alt="profile" class="profile">
				<figcaption class="person-info">
					<h5 class="person-name">${firstName} ${lastName}</h5>
					<h6 class="person-job">${job}</h6>
					<i class="person-loc">${location}</i>
				</figcaption>
			</div>
			<button class="button">Read ${firstName}'s Bio</button>
		</figure>
	</li>`;

	container.appendChild(card.lastChild);
}

function getData(message) {
	return [{
		firstName: "Jacob",
		lastName: "Hawkins",
		job: "Senior Editor",
		location:"New York Times",
		profile: "./imgs/jacob-hawkins.png"
	},
	{
		firstName: "Esther",
		lastName:"Black",
		job: "Editor in Chief",
		location:"Wall Street Journal",
		profile: "./imgs/esther-black.png"
	},
	{
		firstName: "Philip",
		lastName:"Miles",
		job: "Senior Writer",
		location:"Washington Post",
		profile: "./imgs/philip-miles.png"
	}]
}